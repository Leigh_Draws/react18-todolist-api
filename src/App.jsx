import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import TodoList from "./components/TodoList";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]);

  // TODO install and import axios, create an useEffect hook, and call the API

  // Hook useEffect de React qui permet de connecter un composant interne à un élement externe, sur le net comme une API   
  useEffect(() => {
    getTodos();
  }, [])

  // Fonction asynchrone qui récupère l'API à partir de l'URL donnée dans API_URL
  async function getTodos() {

  // Le script attend d'avoir récupéré l'API avant de la stocker dans une constante
  //             avec await         avec axios.get                 avec const response
    const response = await axios.get(API_URL);

  // Utilise le hook setState pour stocker les données de l'API dans le state  
    setTodos(response.data)
  }

// Version raccourcie

// useEffect(() => {
//     axios.get(API_URL).then((response) => {
//       setTodos(response.data)
//     })
//   }, [])

  console.log(todos)

  return (
    <div className="App">
      <TodoList todos={todos} />
      
    </div>
  );
}

export default App;
